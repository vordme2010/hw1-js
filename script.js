//отличия var от let и const
//---
// "const" - задаёт значение которое НЕ МОЖЕТ МЕНЯТЬСЯ в последующим за ним коде (кроме внутренних, допустим таких как значений в массиве или объекте)                                               
// "let" - задаёт значение которое МОЖЕТ МЕНЯТЬСЯ в последующим за ним коде 
// "var" выходит за рамки блочной области видимости, если конкретнее то за пределам функции она будет доступна в отличии от "let" и "const", 
// то есть она может выходить за приделы блоков таких как if, и объявить её можно после этой самой блочной модели и всё будет нормально функционировать. В отличии от "const" и "let". К тому же пременные "let" и "const" - не хоистяться
// мы не можем к ним обращаться пока их не обЪявили в отличии от "var" (кроме случая что мы укажем пременную "let" внутри функции, а потом уже объявим её), 
// так же отличаеться и то как браузер обробатывает данные о коде, в случае с "let" браузер собирает переменные интуитивно человеку - сверху вниз,
// но в кейсе c "var" он пробегает по всему коду ищет и запоминает все var'ы, а потом возвращаеться на начало и выполняет код. (всплытие переменной вверх)
//---
// Почему объявлять переменную через var считается плохим тоном?
// При внесении в код "var" может возникнуть ошибки и недопонимания и сама по себе она (перемнная) устаревшая 
//
//
//
let userName = prompt("tell us your name")
let regexName =  new RegExp(/[^a-zA-Z]/) ;
let regexNameTest = regexName.test(userName) 
let userAge = prompt("type your age")
let regexAge = new RegExp(/[^0-9]/);
let regexAgeTest = regexAge.test(userAge) 
while (userAge.length < 1 || userName.length < 1 || regexNameTest || regexAgeTest){
  alert("Your name or age have some invalid characters or empty brackets, please type its correctly!")
  userName = prompt("enter your name AGAIN")
  regexName =  new RegExp(/[^a-zA-Z]/) ;
  regexNameTest = regexName.test(userName)
  userAge = prompt("enter your age AGAIN")
  regexAge = new RegExp(/[^0-9]/);
  regexAgeTest = regexAge.test(userAge) 
} 
let choice
if (userAge < 18) {
    alert("You are not allowed to visit this website.")
} else if (userAge >= 18 && userAge <= 22){
    choice = confirm("Are you sure you want to continue")
    if (choice == true){
        alert("Welcome, " + userName + "!")
    } else {
        alert("You are not allowed to visit this website.")
    }
} else {
    alert("Welcome, " + userName + "!")
}
